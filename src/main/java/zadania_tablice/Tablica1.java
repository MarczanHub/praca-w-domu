package zadania_tablice;

public class Tablica1 {

    // Napisz metodę, która dla danej tablicy liczb całkowitych zwraca pierwszy element
    //tablicy.
    //first([1,2,3,4]) = 1
    //first(4,9,12,1]) = 4

    public static void main(String[] args) {

        int [] array1 = new int[] {1,2,3,4};
        int firstElement = first(array1);

        System.out.println("Pierwszy element tablicy nr 1 wynosi: " + firstElement);

        int [] array2 = new int[] {4,9,12,1};
        int pierwszyElement = first(array2);

        System.out.println("Pierwszy element tablicy nr 2 wynosi: " + pierwszyElement);
    }
    public static int first(int [] array){


        return array[0];
    }
}
