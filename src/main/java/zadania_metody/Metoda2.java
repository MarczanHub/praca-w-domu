package zadania_metody;

import java.util.Scanner;

public class Metoda2 {

    //Napisz metodę divide wyznaczającą iloraz dwóch zadanych liczb całkowitych.
    //Działanie funkcji sprawdź pisząc odpowiedni program. Dla chętnych *- Napisz
    //metodę z obsługą błędów.

    public static void main(String[] args) {

        divideTwoNumbers(0, 0);

    }
    public static int divideTwoNumbers (int firstNumber, int secondNumber){
        Scanner sc = new Scanner(System.in);
        System.out.println("Program obliczy iloraz dwóch kolejno wprowadzonych liczb całkowitych." + "\n" + "Wprowadź dwie liczby całkowite:");
        firstNumber = sc.nextInt();
        secondNumber = sc.nextInt();
        int result = firstNumber/secondNumber;
        System.out.println("Wynik: " + (firstNumber/secondNumber));


        return result;

    }
}
