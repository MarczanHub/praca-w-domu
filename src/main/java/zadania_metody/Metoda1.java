package zadania_metody;

import java.util.Scanner;

public class Metoda1 {


    //Napisz metodę multiply wyznaczającą iloczyn dwóch zadanych liczb całkowitych.
    //Działanie funkcji sprawdź pisząc odpowiedni program.

    public static void main(String[] args) {
        multiplyTwoNumbers(0, 0);
    }

    public static int multiplyTwoNumbers(int a, int b){
        Scanner sc = new Scanner(System.in);
        System.out.println("Program obliczy iloczyn dwóch wprowadzonych liczb całkowitych." + "\n" + "Wprowadź dwie liczby całkowite:");
        a = sc.nextInt();
        b = sc.nextInt();
        int result = a*b;
        System.out.println("Wynik: " + (a*b));
        return result;

        }


}
