package zadania_string;

public class String1 {
    public static void main(String[] args) {

        // Napisz program, który odczytuje wyraz i sprawdza czy pierwsza litera to "J"
        // Dane: Jacek || Wynik: true
        // Dane2: Tomek || Wynik: false

        String name = "Jacek";
        String name2 = "Tomek";
        name2.startsWith("J");
        name.startsWith("J");


        System.out.println("Dane: " + name + " || " + "Wynik: " + name.startsWith("J"));
        System.out.println("Dane: " + name2 + " || " + "Wynik: " + name2.startsWith("J"));
    }
}
