package zadania_string;

import java.util.Scanner;

public class String2 {
    public static void main(String[] args) {

        // Odczytaj wyraz i wypisz na ekran wartość "true" lub "false"
        // w zależności od tego czy wyraz zawiera w sobie napis "pies".

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj poprawny wyraz");
        String word = sc.nextLine();
        System.out.println(word.contains("pies"));

    }
}