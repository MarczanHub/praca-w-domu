package zadania_pętle;

import java.util.Scanner;

public class Pętla11 {

    // Napisz program, który odczytuje liczbę i sprawdza czy liczba jest pierwsza czy złożona.

    public static boolean liczbaPierwsza(int n){

        for (int i = 2; i < n; i++){

            if (n % i == 0){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        System.out.println("Podaj liczbę:");
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        if (n >= 2 && liczbaPierwsza(n)){
            System.out.println("Podana liczbą jest liczbą pierwszą.");
        } else {
            System.out.println("Podana liczba jest liczbą złożoną.");
        }

    }
}
