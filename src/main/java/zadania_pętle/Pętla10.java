package zadania_pętle;

import java.util.Scanner;

public class Pętla10 {
    public static void main(String[] args) {

        // Napisz program drukujący na ekranie 19 gwiazdek.

        Scanner sc = new Scanner(System.in);
        System.out.println("Program wydrukuje na ekranie 19 gwiazdek" + "\n" + "Oto one:");

        String s = "*";
        for (int i = 1; i <= 19; i++){
            System.out.print(s);
            s = "*";
        }

    }
}
