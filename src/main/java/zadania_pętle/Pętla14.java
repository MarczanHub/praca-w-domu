package zadania_pętle;

import java.util.Scanner;

public class Pętla14 {

    // Napisz program, który wypisuje co drugą literę imienia.

    public static void main(String[] args) {

        System.out.println("Podaj swoje imię: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();


        for (int i = 0; i < name.length(); i++){
            if (i % 2 == 1)
                System.out.print(name.charAt(i)+ " ");
        }

    }

}
