package zadania_pętle;

import java.util.Scanner;

public class Pętla1 {

    public static void main(String[] args) {

        // Napisz program, który wypisuje liczby od 50 do 10 (w tej kolejności)

        for(int number = 50; number >= 10; number--) {
            System.out.println(number);
        }

    }

}
