package zadania_pętle;


import java.util.Scanner;

public class Pętla15 {

    // Napisz program, który oblicza największy wspólny dzielnik dwóch liczb.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Wprowadź pierwszą liczbę:");
        int pierwszaliczba = sc.nextInt();
        System.out.println("Wprowadź drugą liczbę:");
        int drugaliczba = sc.nextInt();

        int a = 0;
        int b = 0;
        if (pierwszaliczba < drugaliczba) {
            b = pierwszaliczba;
            pierwszaliczba = drugaliczba;
            drugaliczba = b;
        }
        for (a = pierwszaliczba;(pierwszaliczba % b != 0 || drugaliczba % b != 0); b--) {
        }
        System.out.println("Największym wspólnym dzielnikiem liczb " + pierwszaliczba + " i " + drugaliczba + " jest " + b);
    }

}
