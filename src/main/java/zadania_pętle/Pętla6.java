package zadania_pętle;

import java.util.Scanner;

public class Pętla6 {
    public static void main(String[] args) {

        // Napisz program, który odczytuje "n" i oblicza "n!".

        Scanner sc = new Scanner(System.in);
        System.out.println("Program będzie obliczał silnię podanej liczby." + "\n" + " Podaj liczbę:");
        int n = sc.nextInt();
        int iloczyn = 1;
        for (int i = 1; i <= n; i++) {
            iloczyn *= i;
        }
        System.out.println("Silnia tej liczby wynosi:" + "\n" + iloczyn);

    }
}
