package zadania_pętle;

import java.util.Scanner;

public class Pętla5 {
    public static void main(String[] args) {

        // Napisz program, który prosi o podanie poprawnego hasła
        // (hasło to "Polska"). Tak długo jak użytkownik nie odgadnie hasła
        // wyświetlany jest komunikat "podaj poprawne hasło".

        Scanner sc = new Scanner(System.in);
        System.out.println("Proszę podać poprawne hasło, aby przejść dalej."+"\n"+"Wpisz hasło:");
        String pass = sc.nextLine();

        while (!pass.equals("Polska")){
            System.out.println("Podaj poprawne hasło:");
            pass = sc.nextLine();
        } if (pass.equals("Polska")){
            System.out.println("Dostęp został przyznany");
        }
    }
}
