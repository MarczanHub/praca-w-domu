package zadania_pętle;

import java.util.Scanner;

public class Pętla7_a {
    public static void main(String[] args) {

        // Napisz program, który wygeneruje za pomocą "*" wieżyczkę (wielkość wieżyczki podaje użytkownik).

        Scanner sc = new Scanner(System.in);
        System.out.println("Narysuj wieżyczkę. Podana liczba będzie oznaczać ilość 'poziomów' wieżyczki." + "\n" + "Podaj liczbę:");
        int lenght = sc.nextInt();

        for (int i = 1; i <= lenght; i++) {
            for (int j = 0; j < i; j++)
                System.out.print("*");
            System.out.println();
        }

    }
}
