package zadania_pętle;

import java.util.Scanner;

public class Pętla8 {

    public static void main(String[] args) {

        // Odczytaj wyraz i wypisz wszystkie cyfry występujące w wyrazie.

        System.out.println("Program wypisze kolejno wszystkie cyfry występujące w wyrazie" + "\n" + "Podaj wyraz zawierający cyfry:");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();

        System.out.println("Wyraz " + s + " zawiera w sobie następujące cyfry:");


        for (int i = 0; i < s.length(); i++) {

            if (Character.isDigit(s.charAt(i)))
                System.out.println(s.charAt(i));

        }
    }
}


