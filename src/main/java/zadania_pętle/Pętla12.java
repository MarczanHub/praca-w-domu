package zadania_pętle;

import java.util.Scanner;

public class Pętla12 {

    // Napisz program, który odczytuje wyraz i wypisuje go w odwrotnej kolejności.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj wyraz: ");
        String s = sc.nextLine();

        String[] words = s.split(" ");
        String revString = " ";

        for (int i = 0; i < words.length; i++){
            String word = words[i];
            String revWord = " ";

            for (int j = word.length() - 1; j >= 0; j--){
                revWord = revWord + word.charAt(j);
            }

            revString = revString + revWord + " ";
        }

        System.out.println(revString);
    }

}
