package zadania_pętle;

import java.util.Scanner;

public class Pętla9 {
    public static void main(String[] args) {

        // Napisz program, który odczytuje wyraz i sprawdza czy wyraz jest palindromem.

        Scanner sc = new Scanner(System.in);
        System.out.println("Program sprawdzi czy podany wyraz jest palindromem" + "\n" + "Podaj wyraz:");
        System.out.println(isPalindrome(sc.nextLine()));
    }
    static boolean isPalindrome(String s){

        for (int i = 0; i < s.length() / 2; i++)
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)){
                return false;
            }
            return true;
    }
}
