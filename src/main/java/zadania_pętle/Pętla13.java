package zadania_pętle;



import java.util.Scanner;

public class Pętla13 {

    //Stwórz program, który odczytuje dany napis i wypisuje ile razy w danym napisie
    //występują małe litery. Przykładowo dla napisu: aAaaBssk wynikiem powinno być
    //6 (małe a występuje 3 razy, s występuje 2 razy, k występuje 1 raz).


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Program odczyta podany przez użytkownika napis i wypisze ile razy w danym napisie występują małe litery." + "\n" + "Podaj napis:");

        String s = sc.nextLine();

        System.out.println("Litera\tIlość Powtórzeń");
        for (char ch = 'a'; ch <= 'z'; ch++){
            int c = 0;
            for (int i = 0; i < s.length(); i++){
                if (ch == s.charAt(i))
                    c++;
            }
            if (c != 0)
                System.out.println(ch + "\t\t" + c);
        }

    }

}
