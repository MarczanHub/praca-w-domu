package zadania_pętle;


import java.util.Scanner;


public class Pętla2 {
    public static void main(String[] args) {

        // Napisz program, który odczytuje "n" i sumuje liczby od 1 do "n".

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int n = sc.nextInt();
        int suma = 0;
        for (int i = 1; i <= n; i++){
            suma += i;
        } if (n ==0) {
            System.out.println("Wpisana liczba musi być większa od 0");
        }
        else{
        System.out.println("Suma: " + suma);}
    }

}
