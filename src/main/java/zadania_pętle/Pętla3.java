package zadania_pętle;

import java.util.Random;
import java.util.Scanner;

public class Pętla3 {
    public static void main(String[] args) {

        // Napisz prostą grę - zadaniem użytkownika będzie zgadnięcie liczby,
        // którą zainicjujemy w programie (przykładowa liczba 600).
        // W przypadku, gdy liczba będzie za duża lub za mała, użytkownik otrzyma
        // odpowiednią odpowiedź. Gramy tak długo dopóki użytkownik zgadnie liczbę.

        System.out.println("Witaj w grze pod tytułem 'Guess Game'!" + "\n"+"Zgadnij jaka liczba została zainicjonwana w programie.");

        

            Scanner sc = new Scanner(System.in);

            int number = 187;
            int guess = 0;

            while (guess != number) {

                System.out.println("Zgadnij poprawną liczbę: ");
                guess = sc.nextInt();

                if (guess > number) {
                    System.out.println("Twoja liczba jest za duża!");
                } else if (guess < number) {
                    System.out.println("Twoja liczba jest za mała!");
                } else {
                    System.out.println("Gratulacje! Zgadłeś poprawną liczbę!");
                    System.exit(0);
                }



            }
        }
    }

